
#include "IRegionSelector/IRoiDescriptor.h"
#include "IRegionSelector/IRegSelSvc.h"
#include "TRT_Cabling/ITRT_CablingSvc.h"
#include "TrigOfflineDriftCircleTool/TrigTRT_DriftCircleProviderTool.h"
#include "TrigOfflineDriftCircleTool/TrigTRT_DCCollByteStreamTool.h"


DECLARE_COMPONENT( TrigTRT_DriftCircleProviderTool )
DECLARE_COMPONENT( TrigTRT_DCCollByteStreamTool )

