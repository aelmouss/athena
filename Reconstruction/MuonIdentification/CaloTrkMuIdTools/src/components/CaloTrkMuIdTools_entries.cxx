#include "CaloTrkMuIdTools/CaloMuonTag.h"
#include "CaloTrkMuIdTools/TrackEnergyInCaloTool.h"
#include "CaloTrkMuIdTools/TrackDepositInCaloTool.h"
#include "CaloTrkMuIdTools/CaloMuonLikelihoodTool.h"

DECLARE_COMPONENT( CaloMuonTag )
DECLARE_COMPONENT( TrackEnergyInCaloTool )
DECLARE_COMPONENT( TrackDepositInCaloTool )
DECLARE_COMPONENT( CaloMuonLikelihoodTool )

